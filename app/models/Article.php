<?php

namespace app\models;

use framework\base\Model;
use \framework\libs\Pagination;

class Article extends Model
{

    public function getArticles() {
        $query = "SELECT id FROM articles";
        $data = $this->allRows($query);
        return count($data);
    }

    public function getArticlesPage(Pagination $pagination,$perPage) {
        $start = $pagination->getStart();
        $query = "SELECT a.id,a.name,a.image,a.short_text,a.date,a.rating,a.cat_id,u.name as login,c.name as category FROM articles a ";
        $query .= "JOIN users u ON u.id=a.user_id ";
        $query .= "JOIN categories c ON c.id=a.cat_id ";
        $query .= "ORDER BY a.date DESC LIMIT $start, $perPage";
        $data = $this->allRows($query);
        return $data;
    }

     public function getAllArticles() {
        $query = "SELECT id,name,date,short_text FROM articles ORDER BY date DESC";
        $data = $this->allRows($query);
        return $data;
    }

    public function updateRating($id) {
        $query = "UPDATE articles SET rating = rating + 1 WHERE id = ? LIMIT 1";
        $result = $this->updateRow($query, [$id]);
        return $result;
    }

    public function getArticle($id) {
        $query = "SELECT id,title,description,keywords,name,image,full_text,date FROM articles WHERE id=?";
        $data = $this->onllyRow($query, [$id]);
        return $data;
    }

    public function getArticlesByCat($id) {
        $query = "SELECT id,name,image,short_text,date FROM articles WHERE cat_id = ? ORDER BY id DESC";
        $data = $this->allRows($query, [$id]);
        return $data;
    }

    public function getSearch($search) {
        $query = "SELECT id,name,image,short_text,date FROM articles WHERE name LIKE '%" . $search . "%' ORDER BY id DESC";
        $data = $this->allRows($query);
        return $data;
    }

    public function getArticleByUserId() {
        $query = "SELECT id,name, date FROM articles WHERE user_id = ? ORDER BY date DESC";
        $data = $this->allRows($query, [$_SESSION['user_id']]);
        return $data;
    }

    public function getArticleForEdit($id,$admin = false) {
        if(!$admin){
        $query = "SELECT id,title,description,keywords,name,image,full_text,short_text,cat_id FROM articles WHERE id=? AND user_id=?";
        $data = $this->onllyRow($query, [$id, $_SESSION['user_id']]);
            
        } else {
            $query = "SELECT id,title,description,keywords,name,image,full_text,short_text,cat_id FROM articles WHERE id=?";
            $data = $this->onllyRow($query, [$id]);
        }
       
        return $data;
    }

    public function validate($data) {
        $data['articlename'] = strip_tags(trim($data['articlename']));
        if (mb_strlen($data['articlename']) == 0) {
            $this->errors[] = 'вы не ввели название';
        }
        $data['articletitle'] = strip_tags(trim($data['articletitle']));
        if (mb_strlen($data['articletitle']) == 0) {
            $this->errors[] = 'вы не ввели title';
        }
        $data['keywords'] = strip_tags(trim($data['keywords']));
        if (mb_strlen($data['keywords']) == 0) {
            $this->errors[] = 'вы не ввели keywords';
        }
        $data['description'] = strip_tags(trim($data['description']));
        if (mb_strlen($data['description']) == 0) {
            $this->errors[] = 'вы не ввели description';
        }
        $data['short_text'] = strip_tags(trim($data['short_text']), '<p>');
        if (mb_strlen($data['short_text']) == 0) {
            $this->errors[] = 'вы не ввели short_text';
        }
        $data['full_text'] = strip_tags(trim($data['full_text']), '<p>');
        if (mb_strlen($data['full_text']) == 0) {
            $this->errors[] = 'вы не ввели full_text';
        }
        if (!empty($this->errors)) {
            return false;
        }
        return $data;
    }

    public function save($data) {
        if ($data['userfile']) {

            $query = "INSERT INTO   articles (name, title, keywords,description,
										  image,user_id,cat_id,short_text,
										  full_text) VALUE (?,?,?,?,?,?,?,?,?)";
            $result = $this->insertRow($query, [$data['articlename'], $data['articletitle'],
                $data['keywords'], $data['description'],
                $data['userfile'], $data['user_id'],
                $data['cat_id'], $data['short_text'], $data['full_text']]
            );
        } else {
            $query = "INSERT INTO   articles (name, title, keywords,description,
										  user_id,cat_id,short_text,
										  full_text) VALUE (?,?,?,?,?,?,?,?)";
            $result = $this->insertRow($query, [$data['articlename'], $data['articletitle'],
                $data['keywords'], $data['description'],
                $data['user_id'],
                $data['cat_id'], $data['short_text'], $data['full_text']]
            );
        }
        return $result;
    }

    public function update($data, $id) {
        if ($data['userfile']) {

            $query = "UPDATE articles SET name = ?, title =?, keywords=?,description=?,
										  image=?,cat_id=?,short_text=?,
										  full_text=? WHERE id=? AND user_id=? LIMIT 1";
            $result = $this->updateRow($query, [$data['articlename'], $data['articletitle'],
                $data['keywords'], $data['description'],
                $data['userfile'], $data['cat_id'], $data['short_text'], $data['full_text'], $id, $_SESSION['user_id']]
            );
        } else {
            $query = "UPDATE articles SET name=?, title=?, keywords=?,description=?,cat_id=?,short_text=?,
										  full_text=? WHERE id=? AND user_id=? LIMIT 1";
            $result = $this->updateRow($query, [$data['articlename'], $data['articletitle'],
                $data['keywords'], $data['description'], $data['cat_id'], $data['short_text'], $data['full_text'], $id, $_SESSION['user_id']]
            );
        }
        return true;
    }
     public function updateAdmin($data, $id) {
        if ($data['userfile']) {

            $query = "UPDATE articles SET name = ?, title =?, keywords=?,description=?,
										  image=?,cat_id=?,short_text=?,
										  full_text=? WHERE id=? LIMIT 1";
            $result = $this->updateRow($query, [$data['articlename'], $data['articletitle'],
                $data['keywords'], $data['description'],
                $data['userfile'], $data['cat_id'], $data['short_text'], $data['full_text'], $id]
            );
        } else {
            $query = "UPDATE articles SET name=?, title=?, keywords=?,description=?,cat_id=?,short_text=?,
										  full_text=? WHERE id=? LIMIT 1";
            $result = $this->updateRow($query, [$data['articlename'], $data['articletitle'],
                $data['keywords'], $data['description'], $data['cat_id'], $data['short_text'], $data['full_text'], $id]
            );
        }
        return true;
    }

    public function delete($id,$admin = false) {
        if(!$admin){
        $query = "DELETE FROM articles WHERE id=? AND user_id=? LIMIT 1";
        $result = $this->deleteRow($query, [$id, $_SESSION['user_id']]);    
        } else {
             $query = "DELETE FROM articles WHERE id=? LIMIT 1";
            $result = $this->deleteRow($query, [$id]);
        }
        return true;
    }

}
