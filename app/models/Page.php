<?php
/**
 * Created by PhpStorm.
 * User: slava
 * Date: 18.07.18
 * Time: 19:50
 */
namespace app\models;

use framework\base\Model;

class Page extends Model
{
    public function getPage($name)
    {
        $query = "SELECT id,name,title, keywords,description,content FROM pages WHERE name = ? LIMIT 1";
        $data = $this->onllyRow($query,[$name]);
        return $data;
    }
}
