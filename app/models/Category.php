<?php
/**
 * Created by PhpStorm.
 * User: slava
 * Date: 20.07.18
 * Time: 15:34
 */

namespace app\models;


use framework\base\Model;

class Category extends Model
{
    public function getCatigories()
    {
        $query = "SELECT  id,name FROM categories ORDER BY name ASC";
        $data = $this->allRows($query);
        return $data;
    }

    public function getCatById($id)
    {
        $query = "SELECT  id,name,title, description, keywords FROM categories WHERE id = ? LIMIT 1";
        $data = $this->onllyRow($query,[$id]);
        return $data;
    }
}