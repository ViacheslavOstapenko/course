<?php

/**
 * Created by PhpStorm.
 * User: slava
 * Date: 20.07.18
 * Time: 21:02
 */

namespace app\models;

use framework\base\Model;

class User extends Model
{

//	private $errors = [];

    public function validate($data) {
        $data['username'] = trim($data['username']);
        if (mb_strlen($data['username']) == 0) {
            $this->errors[] = 'вы не ввели имя';
        }
        $data['useremail'] = trim($data['useremail']);
        if (!preg_match("/[0-9a-z]+@[a-z]/", $data['useremail'])) {
            $this->errors[] = 'неверный email';
        }
        $data['password'] = trim($data['password']);
        if (mb_strlen($data['password']) < 4) {
            $this->errors[] = 'пароль должен быть не мение 4 символов';
        }
        $data['checkpassword'] = trim($data['checkpassword']);
        if ($data['password'] != $data['checkpassword']) {
            $this->errors[] = 'пароли не совпали';
        }
        if (empty($this->errors)) {
            return true;
        }
        return false;
    }

    public function checkUnique($email) {
        $query = "SELECT  email FROM users WHERE email = ?";
        $data = $this->onllyRow($query, [$email]);
        if ($data) {
            $this->errors[] = "email уже сушествоет";
            return false;
        }
        return true;
    }

    public function save($data) {
        $query = "INSERT INTO   users (name, email, password) VALUE (?,?,?)";
        $data = $this->insertRow($query, [$data['username'], $data['useremail'], $data['password']]);
        return true;
    }

    public function getErrors() {
        return $this->errors;
    }

    public function checkUser($email, $password) {
        $query = "SELECT  email,id,name, password FROM users WHERE email = ? AND password = ? AND role = 'user' LIMIT 1";
        $data = $this->onllyRow($query, [$email, $password]);
        if (!$data) {
            $this->errors[] = "email или password неверный";
            return false;
        }
        $_SESSION['user_id'] = $data['id'];
        $_SESSION['user_name'] = $data['name'];
        return true;
    }

    public static function auth() {
        return isset($_SESSION['user_id']);
    }

    public function loginAdmin($email, $password) {
        $query = "SELECT  email,id,name, password FROM users WHERE email = ? AND password = ? AND role = 'admin' LIMIT 1";
        $data = $this->onllyRow($query, [$email, md5($password)]);
        if (!$data) {
            return false;
        }
        $_SESSION['admin_id'] = $data['id'];
        $_SESSION['admin_name'] = $data['name'];
        $_SESSION['role'] = 'admin';
        return TRUE;
    }
        public static function isAdmin() {
        return (isset($_SESSION['admin_id'] ) && $_SESSION['role'] == 'admin');
    }

}

//$