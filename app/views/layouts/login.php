﻿﻿<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="/css/style.css">
        <title>admin enter</title>
    </head>
    <body>

        <div class="center-block-main content">
            <main>
                <?= $content ?>
            </main>
        
    </body>
</html>