﻿﻿<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="/css/style.css">
        <?= $this->getMeta() ?>
    </head>
    <body>
        <header>
            <div class="center-block-main">
                <a href="<?= PATH ?>"><img src="/images/logo.jpg" alt="Company" class="logo"></a>
                <nav>
                    <ul class="menu">
                        <li><a href="<?= PATH ?>">Главная</a></li>
                        <?php if (isset($_SESSION['user_id'])): ?>
                            <li><a href="/user/cabinet">Кобинет</a></li>
                            <li><a href="/user/logout">Выход</a></li>
                        <?php else: ?>
                            <li><a href="/user/registration">Регистрация</a></li>
                            <li><a href="/user/login">Вход</a></li>
                        <?php endif; ?>
                    </ul>
                </nav>
            </div>
        </header>
        <div class="center-block-main content">
            <main>
                <?= $content ?>
            </main>
            <aside>
                <!--<div class="widget">
                    <h2>Search</h2>
                    <form action="/main/search/" method="get">
                        <input name="search" type="search" class="search" placeholder="What are you looking for?">
                    </form>
                </div>-->
                <div class="widget">
                    <h2>Categories</h2>
                    <nav>
                        <ul>
                            <?php
                            $cats = \framework\App::$app->getProperty("cats");
                            foreach ($cats as $cat):?>
                                <li><a href="/category/?id=<?= $cat["id"] ?>"><?= $cat["name"] ?></a></li>
<?php endforeach ?>
                        </ul>
                    </nav>
                </div>
                <!--<div class="widget">
                    <h2>Stay tuned</h2>
                    <div class="subscribe">
                        <form class="subscribe-form" action="" method="get">
                            <input name="subscribe" type="email" placeholder="Your Email" class="subscribe-input subscribe">
                            <input  type="image" class="subscribe-image subscribe-btn" src="/images/sbcr-btn.jpg">
                        </form>
                    </div>
                </div>
                <div class="widget">
                    <img src="/images/banner.jpg" alt="Banner">
                </div>-->
            </aside>
            <div class="clr"></div>
            <!-- <div class="pager clearfix">
                 <p class="previous-link">&larr;&nbsp;<a href="#">Previous</a></p>
                 <p class="next-link"><a href="#">Next</a>&nbsp;&rarr;</p>
             </div>-->
        </div>
        <footer>
            <div class="center-block-main">
                <a href="#"><img src="/images/logo-ftr.jpg" alt=""></a>
                <p>Copyright &copy; 2017 Blogin.com - All right reserved - Find more Templates</p>
            </div>
        </footer>
        <!--spinner-->
        <div id="spinner">
            <div class="loader"></div>
        </div>

        <!-- alert modal -->
        <div id="myModal" class="modal-alert">
            <!-- Modal content -->
            <div class="alert-content">
                <div class="alert-header">
                    <span class="alert-close" onclick="closeAlert()">&times;</span>
                    <h3>Внимание ошибка!</h3>
                </div>
                <div class="alert-body">
                    <p>Непредвиденная ошибка</p>
                </div>
            </div>
        </div>
        <script src="/js/jquery-1.11.0.min.js"></script>
        <script src="/js/myscript.js"></script>
        <script src="/js/main.js"></script>
        <?php if (DEBUG): ?>
            <div class="debuger">
                <?php
                debug(\framework\base\Model::debugger());
                ?>
            </div>
        <?php endif; ?>
    </body>
</html>