<h1>add new article</h1>
<?php
if (isset($_SESSION['errors'])):?>
	<div style="background: red">
		<?php
		foreach ($_SESSION['errors'] as $error):?>
			<p><?= $error ?></p>
		<?php endforeach;
		unset($_SESSION['errors']);
		?>
	</div>
<?php endif; ?>
<?php
if (isset($_SESSION['success'])):
	?>
	<div style="background: #58c93a">
		<p><?= $_SESSION['success'] ?></p>
	</div>
<?php
	unset($_SESSION['success']);
	unset($_SESSION['form_data']);
endif;
?>
<form action="/article/add" method="post" enctype="multipart/form-data">
	<p>имя<input type="text" name="articlename"
	              value="<?= isset($_SESSION['form_data']['articlename']) ? $_SESSION['form_data']['articlename']: '' ?>"></p>
	<p>title<input type="text" name="articletitle" value="<?= isset($_SESSION['form_data']['articletitle']) ? $_SESSION['form_data']['articletitle']: '' ?>"></p>
	<p>keywords<input type="text" name="keywords" value="<?= isset($_SESSION['form_data']['keywords']) ? $_SESSION['form_data']['keywords']: ''?>"></p>
	<p>description<input type="text" name="description" value="<?= isset($_SESSION['form_data']['description']) ? $_SESSION['form_data']['description']: ''?>"></p>
	<p>image<input type="file" name="userfile"></p>
	<select name="cat_id">
		<?php foreach ($categories as $cat):?>
		<option value="<?=$cat['id']?>"><?=$cat['name']?></option>
		<?php endforeach;?>
	</select>
	<p>short text<textarea name="short_text"><?=isset($_SESSION['form_data']['short_text']) ? $_SESSION['form_data']['short_text']: ''?></textarea></p>
	<p>full text<textarea name="full_text"><?=isset($_SESSION['form_data']['full_text']) ? $_SESSION['form_data']['full_text']: ''?></textarea></p>
	<input type="submit" value="отправить">
</form>
