<h1>edit article</h1>
<?php
if (isset($_SESSION['errors']) and !empty($_SESSION['errors'])):?>
    <div style="background: red">
		<?php
		foreach ($_SESSION['errors'] as $error):?>
            <p><?= $error ?></p>
		<?php endforeach;
		unset($_SESSION['errors']);
		?>
    </div>
<?php endif; ?>
<?php
if (isset($_SESSION['success'])):
	?>
    <div style="background: #58c93a">
        <p><?= $_SESSION['success'] ?></p>
    </div>
	<?php
	unset($_SESSION['success']);
	unset($_SESSION['errors']);
endif;
?>
<?php
if (!empty($article)):
	?>
    <form action="" method="post" enctype="multipart/form-data">
        <p>имя<input type="text" name="articlename" value="<?= $article['name'] ?>"></p>
        <p>title<input type="text" name="articletitle" value="<?= $article['title'] ?>"></p>
        <p>keywords<input type="text" name="keywords" value="<?= $article['keywords'] ?>"></p>
        <p>description<input type="text" name="description" value="<?= $article['description'] ?>"></p>
        <img src="/images/<?= $article['image'] ?>" width="200">
        <p>image<input type="file" name="userfile"></p>
        <select name="cat_id">
			<?php foreach ($categories as $cat): ?>
                <option value="<?= $cat['id'] ?>" <?= $cat['id'] == $article['cat_id'] ? 'selected': '' ?>><?= $cat['name'] ?></option>
			<?php endforeach; ?>
        </select>
        <p>short text<textarea name="short_text"><?= $article['short_text'] ?></textarea></p>
        <p>full text<textarea name="full_text"><?= $article['full_text'] ?></textarea></p>
        <input type="submit" value="отправить">
    </form>
<?php else: ?>
    <p>this article not found</p>
<?php endif; ?>


