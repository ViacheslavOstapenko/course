<?php if (!empty($data)): ?>
    <?php foreach ($data as $article): ?>
        <article>
            <header>
                <img src="/images/<?= $article['image'] ?>" alt="Foto1">
                <p class="publish"><?= $article['date'] ?></p>
            </header>
            <h2><?= $article['name'] ?></h2>
            <p class="about">data: <?= $article['date'] ?> | author: <?= $article['login'] ?>
                | category:<a href="/category/?id=<?= $article['cat_id'] ?>"><?= $article['category'] ?></a> | raiting: <?= $article['rating'] ?></p>
            <?= $article['short_text'] ?>
            <p><a href="/article/?id=<?= $article['id'] ?>" class="more">Читать далее...</a></p>
        </article>
    <?php endforeach ?>
    <?php else:
    ?>
    <p>статей на найдено</p>
<?php endif ?>
    <div class="text-center">
        <?php if($pagination->countPages > 1):?>
            <?=$pagination?>
        <?php endif;?>
    </div>

