<h1>welcom to admin Article</h1>
<?php if (isset($_SESSION['errors'])): ?>
    <div style="background: red">
        <?php foreach ($_SESSION['errors'] as $error): ?>
            <p><?= $error ?></p>
        <?php
        endforeach;
        unset($_SESSION['errors']);
        ?>
    </div>
<?php endif; ?>
<?php
if (isset($_SESSION['success'])):
    ?>
    <div style="background: #58c93a">
        <p><?= $_SESSION['success'] ?></p>
    </div>
    <?php
    unset($_SESSION['success']);
    unset($_SESSION['form_data']);
endif;
?>
<?php if (!empty($data)): ?>
    <table border="1">
        <tr>
            <td>#</td>
            <td>name</td>
            <td>date</td>
            <td>delete</td>
        </tr>
    <?php foreach ($data as $key => $article): ?>
            <tr>
                <td><?= $key + 1; ?></td>
                <td><a href="/admin/articles/show/?id=<?= $article['id'] ?>"><?= $article['name'] ?></a></td>
                <td><?= $article['date'] ?></td>
                <td><a onclick="return deleteAction()" href="/admin/articles/delete/?id=<?= $article['id'] ?>">delete article</a></td>
            </tr>
    <?php endforeach; ?>
    </table>
    <?php else: ?>
    <h2>none article</h2>
<?php endif; ?>
