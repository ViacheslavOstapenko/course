
<h1>welcom to admin pinel</h1>
<?php if (isset($_SESSION['errors'])): ?>
    <div style="background: red">
        <?php foreach ($_SESSION['errors'] as $error): ?>
            <p><?= $error ?></p>
        <?php
        endforeach;
        unset($_SESSION['errors']);
        ?>
    </div>
<?php endif; ?>
<?php
if (isset($_SESSION['success'])):
    ?>
    <div style="background: #58c93a">
        <p><?= $_SESSION['success'] ?></p>
    </div>
    <?php
    unset($_SESSION['success']);
    unset($_SESSION['form_data']);
endif;
?>
<?php if (!empty($data)): ?>
    <table border="1">
        <tr>
            <td>#</td>
            <td>text</td>
            <td>date</td>
            <td>email</td>
            <td>delete</td>
        </tr>
    <?php foreach ($data as $key => $comment): ?>
            <tr>
                <td><?= $key + 1; ?></td>
                <td><?= $comment['text'] ?></td>
                <td><?= $comment['date'] ?></td>
                <td><?= $comment['email'] ?></td>
                <td><a onclick="return deleteAction()" href="/admin/comments/delete/?id=<?= $comment['id'] ?>">delete comment</a></td>
            </tr>
    <?php endforeach; ?>
    </table>
    <?php else: ?>
    <h2>none article</h2>
<?php endif; ?>