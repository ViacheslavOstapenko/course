<?php
/*
* Created by PhpStorm.
* User: slava
* Date: 20.07.18
* Time: 20:59
*/
if (isset($_SESSION['errors'])):?>
    <div style="background: red">
		<?php
		foreach ($_SESSION['errors'] as $error):?>
            <p><?= $error ?></p>
		<?php endforeach;
		unset($_SESSION['errors']);
		?>
    </div>
<?php endif; ?>
<?php
if (isset($_SESSION['success'])):
	?>
    <div style="background: #58c93a">
        <p><?= $_SESSION['success'] ?></p>
    </div>
<?php endif;
unset($_SESSION['success']);
?>
<form action="/user/registration" method="post">
    <p>имя <input type="text" name="username"
                  value="<?= isset($_SESSION['form_data']['username']) ? $_SESSION['form_data']['username']: '' ?>"></p>
    <p>email<input type="email" name="useremail" value="<?= isset($_SESSION['form_data']['useremail']) ? $_SESSION['form_data']['useremail']: '' ?>"></p>
    <p>пороль<input type="password" name="password"></p>
    <p>повторить пароль<input type="password" name="checkpassword"></p>
    <input type="submit" value="отправить">
</form>