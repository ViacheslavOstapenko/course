<?php

namespace app\controllers;

use app\models\Category;
use framework\base\Controller;
use framework\App;
use framework\Cache;

class AppController extends Controller
{
    public function __construct($route)
    {
        parent::__construct($route);
        $this->setMeta(App::$app->getProperty("title"), App::$app->getProperty("description"), App::$app->getProperty("keywords"));
        $cats = $this->getListCaterories();
        App::$app->setProperty("cats",$cats);
    }

    //измниеня для списка категорий
    public function getListCaterories()
    {
        $cache = Cache::instance();
        $data = $cache->get("category");
        if(!$data){
            $category = new Category();
            $data= $category->getCatigories();
            $cache->set("category",$data);
        }
        return $data;
    }
}