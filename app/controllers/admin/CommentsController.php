<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers\admin;

/**
 * Description of CommentsController
 *
 * @author slava
 */
use app\models\Comment;

class CommentsController extends AppController
{

    public function indexAction() {
        $comments = new Comment();
        $data = $comments->getAllComments();
        $this->set(compact("data"));
    }

    public function deleteAction() {
        $id = (int)$_GET['id'];
        $comment = new Comment();
        $action = $comment->deleteComment($id);
        redirect();
    }

}
