<?php

/**
 * Created by PhpStorm.
 * User: slava
 * Date: 06.07.18
 * Time: 20:40
 */

namespace app\controllers\admin;

class MainController extends AppController
{

    public function indexAction() {
        $names = ["ana", "jon", "alex"];
        $name = "some string";
        $age = 35;
        $this->set(compact("name", "age", "names"));
        $this->setMeta("Main page", "description", "keywords");
    }

}
