<?php

/**
 * Created by PhpStorm.
 * User: slava
 * Date: 17.07.18
 * Time: 16:47
 */

namespace app\controllers;

use app\models\Article;
use app\models\Category;
use app\models\User;
use app\models\Comment;
use framework\App;
use framework\libs\Pagination;

class ArticleController extends AppController
{

    public function indexAction() {
        if (isset($_GET['id'])) {
            $id = (int) $_GET['id'];
        } else {
            throw new \Exception("page not found", 404);
        }
        $modelComment = new Comment();
        if (!empty($_POST)) {
            $dataForm = $_POST;
            if (!($dataValid = $modelComment->validateComment($dataForm))) {
                $_SESSION['errors'] = $modelComment->getErrors();
                $_SESSION['form_data'] = $dataForm;
            } else {
                $modelComment->save($dataValid);
                $id = $dataValid['articleid'];
            }
        }
        $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        $perPage = App::$app->getProperty('pagination');
        $total = $modelComment->getComments($id);
        $pagination = new Pagination($page, $perPage, $total);
        $comments = $modelComment->getCommentsPage($pagination,$perPage,$id);
        
        $article = new Article();
        $data = $article->getArticle($id);
        $article->updateRating($id);
        
        $this->setMeta($data['title'], $data['description'], $data['keywords']);
        $this->set(compact("data", "comments","pagination","total"));
    }

    public function addAction() {
        if (!User::auth()) {////3
            redirect('/user/login');
        }
        if (!empty($_POST)) {
            $article = new Article();
            $data = $_POST;
            if (!$valid_data = $article->validate($data)) {
                $_SESSION['errors'] = $article->getErrors();
                $_SESSION['form_data'] = $data;
            } else {
                //check file and upload file
                if (!empty($_FILES['userfile']['name'])) {
                    $valid_data['userfile'] = md5(time()) . ".jpg";
                    move_uploaded_file($_FILES['userfile']['tmp_name'], WWW . '/images/' . $valid_data['userfile']);
                    //chenges permission
                } else {
                    $valid_data['userfile'] = false;
                }
                //add article
                $valid_data['user_id'] = $_SESSION['user_id'];

                if ($article->save($valid_data)) {

                    $_SESSION['success'] = 'статья добавлениа ';
                } else {
                    $_SESSION['errors'] = 'ошибка при добовлении стати';
                }
            }
        }
        $cats = new Category();
        $categories = $cats->getCatigories();
        $this->set(compact('categories'));
        $this->setMeta('add new article', 'new', 'newarticle');
    }

    public function editAction() {
        if (!User::auth()) {////3
            redirect('/user/login');
        }

        $id = isset($_GET['id']) ? (int) $_GET['id'] : false;
        if (!$id) {
            throw new \Exception("page not found", 404);
        }
        if (!empty($_POST)) {
            $article = new Article();
            $data = $_POST;
            if (!$valid_data = $article->validate($data)) {
                $_SESSION['errors'] = $article->getErrors();
                $_SESSION['form_data'] = $data;
            } else {
                //check file and upload file
                if (!empty($_FILES['userfile']['name'])) {
                    $valid_data['userfile'] = md5(time()) . ".jpg";
                    move_uploaded_file($_FILES['userfile']['tmp_name'], WWW . '/images/' . $valid_data['userfile']);
                    //chenges permission
                } else {
                    $valid_data['userfile'] = false;
                }
                //add article
                $valid_data['user_id'] = $_SESSION['user_id'];

                if ($article->update($valid_data, $id)) {

                    $_SESSION['success'] = 'статья обновлена ';
                } else {
                    $_SESSION['errors'][] = 'ошибка при обновлении стати';
                }
            }
        }
        $model = new Article();
        $article = $model->getArticleForEdit($id);

        $cats = new Category();
        $categories = $cats->getCatigories();
        $this->set(compact('categories', 'article'));
        $this->setMeta('edit  article', 'edit action', 'edit article');
    }

    public function deleteAction() {
        if (!User::auth()) {////3
            redirect('/user/login');
        }
        $id = isset($_GET['id']) ? (int) $_GET['id'] : false;
        if (!$id) {
            throw new \Exception("page not found", 404);
        }
        $article = new Article();
        $result = $article->delete($id);
        if (!$result) {
            $_SESSION['errors'][] = 'удаление выполнего не было';
        } else {
            $_SESSION['success'] = 'статья успешно удалена';
        }
        redirect();
    }

}
