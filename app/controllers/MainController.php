<?php

/**
 * Created by PhpStorm.
 * User: slava
 * Date: 06.07.18
 * Time: 20:40
 */

namespace app\controllers;

use app\models\Article;
use app\models\Page;
use framework\base\Model;
use framework\Cache;
use framework\libs\Pagination;
use framework\App;

class MainController extends AppController
{

    //public $layout = "test";

    public function indexAction() {
        $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        $perPage = App::$app->getProperty('pagination');
        
        $articles = new Article();
        $total = $articles->getArticles();
        $pagination = new Pagination($page,$perPage,$total);
        $data = $articles->getArticlesPage($pagination,$perPage);
        $page = new Page();
        $pageData = $page->getPage('main');
        $this->setMeta($pageData['title'], $pageData["description"], $pageData["keywords"]);
        $this->set(compact("data","pagination","total"));
    }

    public function searchAction() {
        if (isset($_GET["search"])) {
            $articles = new Article();
            $data = $articles->getSearch($_GET["search"]);
            $page = new Page();
            $pageData = $page->getPage('main');
            $this->setMeta("serch result", $pageData["description"], $pageData["keywords"]);
            $this->set(compact("data"));
        } else {
            throw new \Exception("page not faund", 404);
        }
    }

}
