$('.subscribe-form').on('click', '.subscribe-btn', function(e){
    e.preventDefault();
    var email = $('input.subscribe').val();
 //   var reg = /^[\w\.\d-_]+@[\w\.\d-_]+\.\w{2,4}$/i;

    /*if (!reg.test(email)){
        showAlert('Неверный формат email');
        return;
    }*/
    $.ajax({
        url: '/subscribe',
        data: {subscribe: email},
        type: 'GET',
        beforeSend: function(){
            $("#spinner").fadeIn(200);
        },
        success: function(res){
            $("#spinner").css('display', 'none');
            if(res.success){
                showAlert(res.message, 'success');
            }else{
                showAlert(res.error, 'error');
            }
        },
        error: function(){
            $("#spinner").css('display', 'none');
            showAlert('Ошибка!');
        }
    });
});


function closeAlert(){
    $(".modal-alert").fadeOut();
}

function showAlert(msg, type = 'error'){
    var color = '';
    $(".modal-alert p").text(msg);
    if(type == 'success'){
        color = '#2E8B57';
        $(".alert-header").css('background', color);
        $(".modal-alert h3").text('Сообщение!');
        $(".alert-body").css('color', color);
    } else{
        color = '#CD5C5C';
        $(".alert-header").css('background', color);
        $(".modal-alert h3").text('Внимание ошибка!');
        $(".alert-body").css('color', color);
    }
    $(".modal-alert").fadeIn();
}
